package net.sytes.rokkosan.smbclient

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import jcifs.CIFSContext
import jcifs.config.PropertyConfiguration
import jcifs.context.BaseContext
import jcifs.smb.NtlmPasswordAuthentication
import jcifs.smb.SmbFile
import kotlinx.coroutines.*
import net.sytes.rokkosan.smbclient.databinding.ActivityMainBinding
import java.util.*
import kotlin.coroutines.CoroutineContext


class MainActivity : AppCompatActivity(), CoroutineScope {
    //////////////////////////////////////////////
    // Please replace these values to your data //
    val DOMAIN: String = "192.168.1.1"
    val USER: String = "user"
    val SMBROOT: String = "/samba/server/path"
    //////////////////////////////////////////////

    val TAG: String = "MyApp"
    lateinit var binding: ActivityMainBinding
    lateinit var domain: String
    lateinit var user: String
    lateinit var password: String
    lateinit var smbroot: String

    // coroutine準備
    private val job = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    // 終了時のcoroutineのキャンセル設定
    override fun onDestroy() {
        job.cancel()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.edTxtDomain.setText(DOMAIN)
        binding.edTxtAccount.setText(USER)
        binding.edTxtSmbRoot.setText(SMBROOT)

        binding.btnConnect.setOnClickListener {

            // ソフトキーボードを非表示にする
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(it.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS)

            binding.txtVw.setText(R.string.wait)

            // 認証情報取得
            if ( binding.edTxtDomain.text.length == 0 ) {
                binding.txtVw.text = "Please set domain"
                return@setOnClickListener
            } else {
                domain = binding.edTxtDomain.text.toString()
            }
            if ( binding.edTxtAccount.text.length == 0 ) {
                binding.txtVw.text = "Please set account"
                return@setOnClickListener
            } else {
                user = binding.edTxtAccount.text.toString()
            }
            if ( binding.edTxtPassword.text.length == 0 ) {
                binding.txtVw.text = "Please set password"
                return@setOnClickListener
            } else {
                password = binding.edTxtPassword.text.toString()
            }
            if ( binding.edTxtSmbRoot.text.length == 0 ) {
                binding.txtVw.text = "Please set root-path of SMB-server"
                return@setOnClickListener
            } else {
                smbroot = "smb://" + domain + binding.edTxtSmbRoot.text.toString()
                if (smbroot.takeLast(1) != "/") {
                    smbroot = smbroot + "/"
                }
            }

            // smbアクセス処理はActivityとは別スレッドとする必要があるのでcoroutineを利用する
              launch {
                val deferred = async(Dispatchers.IO) {
                    val prop = Properties()
                    prop.setProperty("jcifs.smb.client.minVersion", "SMB202")
                    prop.setProperty("jcifs.smb.client.maxVersion", "SMB300")
                    val bc = BaseContext(PropertyConfiguration(prop))
                    val creds = NtlmPasswordAuthentication(bc, domain, user, password)
                    val auth: CIFSContext = bc.withCredentials(creds)
                    val smb = SmbFile(smbroot, auth)
                    var smbPaths: String = ""
                    if (smb.exists()) {
                        var smbFiles = smb.listFiles();
                        for (smbFile in smbFiles) {
                            Log.d(TAG, smbFile.getName())
                            smbPaths += "${smbFile} "
                        }
                    }
                    smbPaths
                }
                withContext(Dispatchers.Main) {
                    binding.txtVw.setText(deferred.await())
                }
            }
        }
    }
}
